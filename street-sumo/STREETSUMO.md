## STREET SUMO

It's a rythm match game, where the key press timing is necessary to win the challenge. The challenge replicate
a Sumo match.
Main goal is not to be push out of the arena.

### Rules

The player in this phase is a geometric form (square). The only loosing condition is, if the player gets out of the arena.
___
### TO DO
- [ ] Create `Menu`;
- [ ] Add `pause` function;
- [ ] For debug purpose, add a element in the browser showing important values like, `speed`, `distance from arena borders`, `delta time from each key pressed or matcher`
- [ ] Add gravity engine to simulate physics arena falls;
- [ ] Engine to display random keys, to match with key press;
- [ ] Refactor the `setup` data models (ARENA, PLAYER, GAME,...), so it doesn't have explicit values;

___
### Future Capabilities

- [ ] Use `balance` as a condition of the player, with this is possible to leverage the excess speed,
of the adversary and take advantage;
- [ ] Add `hit boxes` for representing `arms` `legs` `body`, for example it can have a direct impact on the `balance`;
- [ ] `Especial combos or moves`, whenever a player can match perfectly a set of keys, it will be available a especial move.
- [ ] Switch side on the arena, in certain match conditions.