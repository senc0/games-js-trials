
class Player {
    constructor(ctx, width, height, x_pos, y_pos) {
        this._ctx = ctx;
        this._width = width;
        this._height = height;
        this._x_pos = x_pos; 
        this._y_pos = y_pos; 
    }

    update(input_state) {

        // CHANGE STATE
        this._x_pos += input_state['right'];
        this._x_pos -= input_state['left'];
    }

    draw() {
        this._ctx.beginPath();
        this._ctx.rect(this._x_pos, this._y_pos, this._width, this._height);
        this._ctx.fillStyle = 'black';
        this._ctx.fill();
    }

    getPostion() {
		// attending  the size of the object/player, what is given
		// its center position in the arena
        return {
            rightPos: this._x_pos // the subtration is 
        }
    }
}