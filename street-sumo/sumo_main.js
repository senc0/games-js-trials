var ARROW_MAP = {
    37: 'left',
    39: 'right'
};

var GAME_SETUP = {
	'width': 800,
	'height': 300,
	'speed': 5
}

// the intention is, all the `objects` within
// main canvas must be calculated through ratios.

var ARENA_SETUP = {
	'xx': GAME_SETUP['width'] / 4,
	'yy': GAME_SETUP['height'] / 1.2,
	'width': GAME_SETUP['width'] / 2,
	'height': GAME_SETUP['height'] / 6,
}

var PLAYER_SETUP = {
	'xx': ARENA_SETUP['xx'] + 50 ,
	'yy': ARENA_SETUP['yy'] / 1.68,
	'width': 100,
	'height': 100
}

// this will set a left movement to use on a bot
// or simulation of opposing force
var GHOST_FORCE = {
	'left': true,
	'right': false
}

class Game {
	constructor(canvas, width, height, speed) {
		canvas.width = width;
		canvas.height = height;
		this._width = width;
		this._height = height;
		this._speed = speed;
		this._ctx = canvas.getContext('2d');
        this._player = new Player(this._ctx, PLAYER_SETUP['width'], PLAYER_SETUP['height'], PLAYER_SETUP['xx'], PLAYER_SETUP['yy']);
        this._input_state = {
            'left': false,
            'right': false
		}
		this._position_update = { 
			'left': 0,
			'right': 0
		}
        document.addEventListener('keydown', this.keydown.bind(this))
		document.addEventListener('keyup', this.keyup.bind(this))
	}

	play() {
		// update game state
		this._eventUpdate(this._input_state);
		this._player.update(this._position_update);
        // draw game
		this._clear();
		this._drawBorder();
		this._drawArena();
		this._player.draw();
		
		// ghost force that will push the box on opposite direction
		//this._player.update(GHOST_FORCE);
		//this._player.draw();

		if (this._checkState()) {
			requestAnimationFrame(this.play.bind(this));
		} else {
			this._playLose();
		}
	}

	_eventUpdate() {
		 // HANDLE INPUT
		 this._position_update['left'] = 0;
		 if (this._input_state['left']) {
			this._position_update['left'] += this._speed;
		 }
		 this._position_update['right'] = 0;
		 if (this._input_state['right']) {
			this._position_update['right'] += this._speed;
		 }
	}

	 _checkState() {
		let borders = this._player.getPostion();
		// check if the player is within the arena limits11
		return (borders['rightPos'] > 100 && borders['rightPos'] < 500);
    }

    _playLose() {
        this._ctx.beginPath();
        this._ctx.font = '32px serif';
        this._ctx.fillStyle = 'red';
        this._ctx.fillText("YAMERO!", this._width / 6, this._height / 6);
    }

    _drawBorder() {
        this._ctx.beginPath();
        this._ctx.rect(0, 0, this._width, this._height);
        this._ctx.stroke();
	}
	
	_drawArena() {
		this._ctx.beginPath();
		this._ctx.rect(ARENA_SETUP['xx'], ARENA_SETUP['yy'], ARENA_SETUP['width'] , ARENA_SETUP['height']);
		this._ctx.stroke();
	}

    _clear() {
        this._ctx.clearRect(0, 0, this._width, this._height); // just clear the whole game area
    }

    keydown(e) {
        let arrow = ARROW_MAP[e.keyCode];

        if (arrow === 'left') {
            this._input_state['left'] = true;
        }
        if (arrow === 'right') {
            this._input_state['right'] = true;
        }
    }

    keyup(e) {
        let arrow = ARROW_MAP[e.keyCode];

        if (arrow === 'left') {
            this._input_state['left'] = false;
        }
        if (arrow === 'right') {
            this._input_state['right'] = false;
        }
    }
}

var game = new Game(document.getElementsByTagName('canvas')[0], GAME_SETUP['width'], GAME_SETUP['height'], GAME_SETUP['speed']); // create an instance of the game
game.play(); // start it