var ARROW_MAP = {
    37: 'left',
    40: 'up',
    39: 'right',
    38: 'down'
};

class Player {
    constructor(ctx, width, height, speed) {
        this._ctx = ctx;
        this._width = width;
        this._height = height;
        this._x = 0;
        this._y = 0;
        this._speed = speed; //set default player speed
        this._speed_x = 0;
        this._speed_y = 0;
    }

    update(input_state) {

        // HANDLE INPUT

        var new_speed_x = 0;
        if (input_state['left']) {
            new_speed_x += -this._speed;
        }
        if (input_state['right']) {
            new_speed_x += this._speed;
        }

        var new_speed_y = 0;
        if (input_state['up']) {
            new_speed_y += this._speed;
        }
        if (input_state['down']) {
            new_speed_y += -this._speed;
        }
        
        this._speed_x = new_speed_x;
        this._speed_y = new_speed_y;

        // CHANGE STATE

        this._x += this._speed_x;
        this._y += this._speed_y;
    }

    draw() {
        this._ctx.beginPath();
        this._ctx.rect(this._x, this._y, this._width, this._height);
        this._ctx.fillStyle = 'yellow';
        this._ctx.fill();
    }

    getBorders() {
        return {
            xMin: this._x,
            xMax: this._x + this._width,
            yMin: this._y,
            yMax: this._y + this._height,
        }
    }
}