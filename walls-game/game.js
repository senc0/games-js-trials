class Game {
	constructor(canvas, width, height) {
		canvas.width = width;
		canvas.height = height;
		this._width = width;
		this._height = height;
		this._ctx = canvas.getContext('2d');
        this._player = new Player(this._ctx, this._width / 10, this._height / 10, 5);
        this._input_state = {
            'up': false,
            'down': false,
            'left': false,
            'right': false
        }
        document.addEventListener('keydown', this.keydown.bind(this))
        document.addEventListener('keyup', this.keyup.bind(this))
		this._player = new Player(this._ctx, this._width / 10, this._height / 100);
	}

	play() {
        // update game state
        this._player.update(this._input_state)

        // draw game
		this._clear();
		this._drawBorder();
		this._player.draw();

		if (this._checkState()) {
			requestAnimationFrame(this.play.bind(this));
		} else {
			this._playLose();
		}
	}

	 _checkState() {
        let borders = this._player.getBorders();
        return (borders.xMin >= 0 &&
            borders.xMax <= this._width &&
            borders.yMin >= 0 &&
            borders.yMax <= this._height);
    }

    _playLose() {
        this._ctx.beginPath();
        this._ctx.font = '48px serif';
        this._ctx.fillStyle = 'red';
        this._ctx.fillText("You lose!", this._width / 2, this._height / 2);
    }

    _drawBorder() {
        this._ctx.beginPath();
        this._ctx.rect(0, 0, this._width, this._height);
        this._ctx.stroke();
    }

    _clear() {
        this._ctx.clearRect(0, 0, this._width, this._height); // just clear the whole game area
    }

    keydown(e) {
        let arrow = ARROW_MAP[e.keyCode];

        if (arrow === 'left') {
            this._input_state['left'] = true;
        }
        if (arrow === 'right') {
            this._input_state['right'] = true;
        }
        if (arrow === 'up') {
            this._input_state['up'] = true;
        }
        if (arrow === 'down') {
            this._input_state['down'] = true;
        }
    }

    keyup(e) {
        let arrow = ARROW_MAP[e.keyCode];

        if (arrow === 'left') {
            this._input_state['left'] = false;
        }
        if (arrow === 'right') {
            this._input_state['right'] = false;
        }
        if (arrow === 'up') {
            this._input_state['up'] = false;
        }
        if (arrow === 'down') {
            this._input_state['down'] = false;
        }
    }
}

var game = new Game(document.getElementsByTagName('canvas')[0], 400, 400); // create an instance of the game
game.play(); // start it
